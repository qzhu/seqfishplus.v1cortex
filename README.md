### File listing
```
-rw-r--r-- 1 qzhu qzhu 86343 May  9 03:53 tasic.class.9.labels
-rw-r--r-- 1 qzhu qzhu 63639 May  9 03:53 tasic.class.22.labels
-rw-r--r-- 1 qzhu qzhu 70567 May  9 03:53 tasic.class.49.labels
-rw-r--r-- 1 qzhu qzhu 18133 May  9 03:54 seqfish.1e-5
-rw-r--r-- 1 qzhu qzhu  1107 May  9 03:57 unsupervised
```

### Explanations
*  tasic.class.9.labels: 9-class Tasic et al scRNAseq cell types (each row is a cell in Tasic dataset)
*  tasic.class.22.labels: 22-class Tasic et al scRNAseq cell types
*  tasic.class.49.labels: 49-class Tasic et al scRNAseq cell types
*  seqfish.1e-5: predicted 22-class labels on seqfish+ visual cortex dataset (via SVM) (each row is a cell)
*  unsupervised: unsupervised clustering results

### Format of tasic.class.9.labels:
Col1: 9-class label, Col2: Cell.ID, Col3: dissected layer

### Format of tasic.class.22.labels:
Col1: 22-class label, Col2: Cell.ID, Col3: dissected layer

### Format of tasic.class.49.labels:
Col1: 49-class label, Col2: Cell.ID, Col3: dissected layer

### Format of seqfish.1e-5:
Col1: Cell.ID, Col2: (ignore), Col3: predicted label (22-class), Col4: (ignore), Col5: decision function score, Col6: mapping probability

### Format of unsupervised:
Cluster ID of unsupervised clustering
